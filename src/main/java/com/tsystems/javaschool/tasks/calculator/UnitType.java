package com.tsystems.javaschool.tasks.calculator;

public enum UnitType {
    NUMBER,
    OPERATOR,
    BRACE_OPEN,
    BRACE_CLOSE,
    UNKNOWN
}
