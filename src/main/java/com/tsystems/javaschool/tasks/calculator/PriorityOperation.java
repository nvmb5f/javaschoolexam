package com.tsystems.javaschool.tasks.calculator;

public class PriorityOperation {

    private final int priority;
    private final Operation operation;

    public PriorityOperation(int priority, Operation operation) {
        this.priority = priority;
        this.operation = operation;
    }

    public int getPriority() {
        return priority;
    }

    public Operation getOperation() {
        return operation;
    }
}