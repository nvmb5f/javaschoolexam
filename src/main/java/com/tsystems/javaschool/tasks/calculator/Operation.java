package com.tsystems.javaschool.tasks.calculator;

@FunctionalInterface
public interface Operation {

    double calculate(double op1, double op2);
}
