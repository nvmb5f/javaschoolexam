package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.util.*;

public class Calculator {

    private static final Map<String, PriorityOperation> OPERATIONS = new HashMap<>();
    private static final String REGEX_NUMBER = "^\\d+(\\.\\d+)?$";

    static {
        OPERATIONS.put("-", new PriorityOperation(1, (a, b) -> a - b));
        OPERATIONS.put("+", new PriorityOperation(1, (a, b) -> a + b));
        OPERATIONS.put("*", new PriorityOperation(2, (a, b) -> a * b));
        OPERATIONS.put("/", new PriorityOperation(2, (a, b) -> a / b));
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (!isValid(statement)) {
            return null;
        }

        statement = statement.replaceAll("\\s", "");
        StringTokenizer tokenizer = new StringTokenizer(statement, getDelimiters(), true);

        List<String> expression = new ArrayList<>(tokenizer.countTokens());

        while (tokenizer.hasMoreTokens()) {

            String token = tokenizer.nextToken();

            if (defineUnitType(token) == UnitType.UNKNOWN) {
                return null;
            }
            expression.add(token);
        }

        String result;
        try{
            double res = processExpression(expression);

            if (Double.isNaN(res) || Double.isInfinite(res)){
                result = null;

            } else{
                result = BigDecimal
                        .valueOf(res)
                        .setScale(4, BigDecimal.ROUND_HALF_UP)
                        .stripTrailingZeros()
                        .toPlainString();
            }
        } catch (Exception e){
            result = null;
        }

        return result;
    }

    private double processExpression(List<String> expression) {

        List<String> convertedExpression = convertToRPN(expression);
        return calculateExpressionInRPN(convertedExpression);
    }

    private double calculateExpressionInRPN(List<String> expression) {
        ArrayDeque<Double> resultStack = new ArrayDeque<>();

        for (String unit : expression) {

            UnitType type = defineUnitType(unit);

            switch (type) {
                case NUMBER:
                    resultStack.push(Double.parseDouble(unit));
                    break;
                case OPERATOR:
                    double op2 = resultStack.pop();
                    double op1 = resultStack.pop();
                    resultStack.push(OPERATIONS.get(unit).getOperation().calculate(op1, op2));
                    break;
                default:
                    throw new IllegalArgumentException("Invalid RPN expression");
            }
        }

        return resultStack.pop();
    }

    private List<String> convertToRPN(List<String> expression) {

        List<String> notation = new ArrayList<>(expression.size());
        ArrayDeque<String> stack = new ArrayDeque<>();

        for (String unit : expression) {

            UnitType type = defineUnitType(unit);

            switch (type) {
                case NUMBER:
                    notation.add(unit);
                    break;
                case OPERATOR:
                    while (!stack.isEmpty()
                            && defineUnitType(stack.peek()) == UnitType.OPERATOR
                            && OPERATIONS.get(unit).getPriority() <= OPERATIONS.get(stack.peek()).getPriority()) {
                        notation.add(stack.pop());
                    }
                    stack.push(unit);
                    break;
                case BRACE_OPEN:
                    stack.push(unit);
                    break;
                case BRACE_CLOSE:
                    while (!stack.isEmpty()
                            && defineUnitType(stack.peek()) != UnitType.BRACE_OPEN) {
                        notation.add(stack.pop());
                    }
                    if (stack.isEmpty()){
                        throw new IllegalStateException("Parentheses mismatch");
                    }
                    stack.pop();
                    break;
                default:
                    throw new IllegalArgumentException();
            }
        }

        while (!stack.isEmpty()) {
            notation.add(stack.pop());
        }

        return notation;
    }

    private UnitType defineUnitType(String str) {

        switch (str) {
            case "(":
                return UnitType.BRACE_OPEN;
            case ")":
                return UnitType.BRACE_CLOSE;
            case "+":
            case "-":
            case "*":
            case "/":
                return UnitType.OPERATOR;
            default:
                return str.matches(REGEX_NUMBER) ? UnitType.NUMBER : UnitType.UNKNOWN;

        }
    }

    private String getDelimiters() {
        return String.join("", OPERATIONS.keySet()) + ")(";
    }

    private boolean isValid(String statement) {
        return statement != null
                && !statement.trim().equals("")
                && statement.matches("^[)(\\d\\s\\-+/*.]+$");
    }
}
