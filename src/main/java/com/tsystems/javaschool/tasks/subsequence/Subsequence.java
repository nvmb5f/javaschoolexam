package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        int count = x.size();

        Iterator iterator = y.iterator();

        for (Object item : x) {
            while (count > 0 && iterator.hasNext()) {
                if (iterator.next().equals(item)) {
                    --count;
                    break;
                }
            }
        }

        return count == 0;
    }
}
