package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int size = inputNumbers.size();

        int nRows;
        if ((nRows = getRows(size)) == -1) {
            throw new CannotBuildPyramidException();
        }

        int nColumns = 2 * nRows - 1;
        int[][] pyramid = new int[nRows][nColumns];

        Collections.sort(inputNumbers);
        Collections.reverse(inputNumbers);

        int offset = 0;
        int middle = nRows - 1;

        for (int nRow = 0; nRow < pyramid.length; nRow++) {

            int rowNumbersCount = nRow + 1;
            int startIndex = middle - nRow;

            while (rowNumbersCount-- > 0) {
                pyramid[nRow][startIndex + 2 * offset] = inputNumbers.get(--size);
                offset++;

            }
            offset = 0;
        }

        return pyramid;
    }

    private int getRows(int size) {
        int nRows = -1;
        double sqrt = Math.sqrt(1 + 8 * size);
        if (sqrt == (int) sqrt) {
            nRows = (int) Math.abs((1 - sqrt) / 2);
        }
        return nRows;
    }
}
